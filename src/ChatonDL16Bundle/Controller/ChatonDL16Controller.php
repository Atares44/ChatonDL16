<?php

namespace ChatonDL16Bundle\Controller;

use ChatonDL16Bundle\Entity\ChatonDL16;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Chatondl16 controller.
 *
 * @Route("/")
 */
class ChatonDL16Controller extends Controller
{
    /**
     * Lists all chatonDL16 entities.
     *
     * @Route("/", name="chatondl16_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $chatonDL16s = $em->getRepository('ChatonDL16Bundle:ChatonDL16')->findAll();

        return $this->render('chatondl16/index.html.twig', array(
            'chatonDL16s' => $chatonDL16s,
        ));
    }

    /**
     * Creates a new chatonDL16 entity.
     *
     * @Route("/new", name="chatondl16_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $chatonDL16 = new Chatondl16();
        $form = $this->createForm('ChatonDL16Bundle\Form\ChatonDL16Type', $chatonDL16);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($chatonDL16);
            $em->flush($chatonDL16);

            return $this->redirectToRoute('chatondl16_show', array('id' => $chatonDL16->getId()));
        }

        return $this->render('chatondl16/new.html.twig', array(
            'chatonDL16' => $chatonDL16,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a chatonDL16 entity.
     *
     * @Route("/{id}", name="chatondl16_show")
     * @Method("GET")
     */
    public function showAction(ChatonDL16 $chatonDL16)
    {
        $deleteForm = $this->createDeleteForm($chatonDL16);

        return $this->render('chatondl16/show.html.twig', array(
            'chatonDL16' => $chatonDL16,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing chatonDL16 entity.
     *
     * @Route("/{id}/edit", name="chatondl16_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ChatonDL16 $chatonDL16)
    {
        $deleteForm = $this->createDeleteForm($chatonDL16);
        $editForm = $this->createForm('ChatonDL16Bundle\Form\ChatonDL16Type', $chatonDL16);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('chatondl16_edit', array('id' => $chatonDL16->getId()));
        }

        return $this->render('chatondl16/edit.html.twig', array(
            'chatonDL16' => $chatonDL16,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a chatonDL16 entity.
     *
     * @Route("/{id}", name="chatondl16_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ChatonDL16 $chatonDL16)
    {
        $form = $this->createDeleteForm($chatonDL16);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($chatonDL16);
            $em->flush($chatonDL16);
        }

        return $this->redirectToRoute('chatondl16_index');
    }

    /**
     * Creates a form to delete a chatonDL16 entity.
     *
     * @param ChatonDL16 $chatonDL16 The chatonDL16 entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ChatonDL16 $chatonDL16)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('chatondl16_delete', array('id' => $chatonDL16->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
