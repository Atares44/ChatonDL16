<?php

namespace ChatonDL16Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ChatonDL16
 *
 * @ORM\Table(name="chaton_d_l16")
 * @ORM\Entity(repositoryClass="ChatonDL16Bundle\Repository\ChatonDL16Repository")
 */
class ChatonDL16
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="qualite", type="string", length=255)
     */
    private $qualite;

    /**
     * @var string
     *
     * @ORM\Column(name="marqueCroquettes", type="string", length=255)
     */
    private $marqueCroquettes;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return ChatonDL16
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set qualite
     *
     * @param string $qualite
     * @return ChatonDL16
     */
    public function setQualite($qualite)
    {
        $this->qualite = $qualite;

        return $this;
    }

    /**
     * Get qualite
     *
     * @return string 
     */
    public function getQualite()
    {
        return $this->qualite;
    }

    /**
     * Set marqueCroquettes
     *
     * @param string $marqueCroquettes
     * @return ChatonDL16
     */
    public function setMarqueCroquettes($marqueCroquettes)
    {
        $this->marqueCroquettes = $marqueCroquettes;

        return $this;
    }

    /**
     * Get marqueCroquettes
     *
     * @return string 
     */
    public function getMarqueCroquettes()
    {
        return $this->marqueCroquettes;
    }
}
